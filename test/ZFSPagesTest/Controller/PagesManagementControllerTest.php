<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 04.03.15
 * Time: 15:48
 */

namespace ZFS\Pages\Test\Controller;

use ZFS\Pages\Test\Bootstrap;
use ZFS\Pages\Controller\PagesManagementController;
use Zend\Mvc\Router\RouteMatch;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use ZFS\Pages\Model\Object\Page as PageObject;
use Zend\Stdlib\Parameters;
use ZFS\Pages\Test\Service\Service;

class PagesManagementControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \ZFS\Pages\Controller\PagesManagementController */
    protected $controller;
    /** @var $request \Zend\Http\Request */
    protected $request;
    /** @var $routeMatch \Zend\Mvc\Router\RouteMatch */
    protected $routeMatch;
    /** @var $event \Zend\Mvc\MvcEvent */
    protected $event;
    /** @var $service \ZFS\Pages\Test\Service\Service */
    protected $service;

    protected static $storage;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();

        $this->controller = new PagesManagementController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'pagesmanagement'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        $serviceManager->setAllowOverride(true);

        $this->service = new Service($this->controller);

        $this->setUpAuthService();

        $this->service
            ->setUpIsGrantedPlugin()
            ->setUpSocialStorage()
            ->setUpPagesTS();
    }

    /**
     * @return $this
     */
    protected function setUpAuthService()
    {
        $tableGateway = $this->getMockBuilder('Zend\Authentication\AuthenticationService')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('getIdentity')
            ->will($this->returnValue((object)array('userId' => '1')));

        $this->controller->getServiceLocator()->setService('ZFS\AuthService', $tableGateway);

        return $this;
    }

    /**
     * @param mixed $objectToReturn
     * @return $this
     */
    protected function setUpPageGateway($objectToReturn = null)
    {
        $tableGateway = $this->getMockBuilder('ZFS\Pages\Model\Gateway\PageGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('getPages')
            ->will($this->returnValue($objectToReturn));

        $tableGateway->expects($this->any())
            ->method('selectOne')
            ->will($this->returnCallback(function ($where) use ($objectToReturn) {
                if (isset($where['id']) && $where['id'] == static::$storage->id) {
                    return static::$storage;
                }

                if (isset($where['email'])) {
                    return $objectToReturn;
                }

                return null;
            }));

        $tableGateway->expects($this->any())
            ->method('getAdapter')
            ->will($this->returnCallback(function () {
                return $this->service->getAdapterMock();
            }));

        $tableGateway->expects($this->any())
            ->method('delete')
            ->will($this->returnCallback(function ($where) {
                if ($where['id'] == static::$storage->id) {
                    static::$storage = null;
                }
            }));

        $tableGateway->expects($this->any())
            ->method('updateObject')
            ->will($this->returnCallback(function ($object) {
                static::$storage = $object;
            }));

        $tableGateway->expects($this->any())
            ->method('insertObject')
            ->will($this->returnCallback(function ($object) {
                static::$storage = $object;
            }));

        $this->controller->getServiceLocator()->setService('PageGateway', $tableGateway);

        return $this;
    }

    public function testIndexAction()
    {
        $this->setUpPageGateway();

        $this->routeMatch->setParam('action', 'index');

        $this->controller->dispatch($this->request);

        $this->assertEquals(200, $this->controller->getResponse()->getStatusCode());
    }


    public function testCreateActionNewPage()
    {
        $this->setUpPageGateway();

        $this->service->setUpIdentityPlugin((object)array('userId' => 1));

        $this->routeMatch->setParam('action', 'create');

        $post = new Parameters(array(
            'title' => 'foo',
            'slug'  => 'bar',
            'content' => 'foobarbaz',
            'keywords' => '_foo, _bar',
            'description' => '_foo _bar _baz'
        ));

        $expected = new PageObject();
        $expected->userId = $this->controller->getServiceLocator()->get('ZFS\AuthService')->getIdentity()->userId;
        $expected->fromArray($post->toArray());

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $this->controller->dispatch($this->request);

        $this->assertEquals($expected, static::$storage);
        $this->assertEquals(302, $this->controller->getResponse()->getStatusCode());

        static::$storage->id = '1';
    }

    public function testCreateActionUpdatePage()
    {
        $this->setUpPageGateway();

        $this->service->setUpIdentityPlugin((object)array('userId' => 1));

        $this->routeMatch->setParam('action', 'create');

        $post = new Parameters();
        $post->fromArray(static::$storage->toArray());
        $post->set('slug', 'buz');

        $this->request->setMethod(Request::METHOD_POST)->setPost($post);

        $this->assertNull(static::$storage->updated);

        $this->controller->dispatch($this->request);

        $this->assertNotNull(static::$storage->updated);

        $this->assertEquals(302, $this->controller->getResponse()->getStatusCode());
    }

    public function testCreateActionEdit()
    {
        $this->setUpPageGateway();

        $this->routeMatch->setParam('action', 'create');
        $this->routeMatch->setParam('id', static::$storage->id);

        $this->request->setMethod(Request::METHOD_GET);

        $this->controller->dispatch($this->request);

        $this->assertEquals(200, $this->controller->getResponse()->getStatusCode());
    }

    public function testDeleteAction()
    {
        $this->setUpPageGateway();

        $this->routeMatch->setParam('action', 'delete');
        $this->routeMatch->setParam('id', static::$storage->id);

        $this->assertNotNull(static::$storage);

        $storage = static::$storage;

        $this->controller->dispatch($this->request);

        $this->assertEquals(302, $this->controller->getResponse()->getStatusCode());

        $this->assertNull(static::$storage);

        static::$storage = $storage;
    }
}
