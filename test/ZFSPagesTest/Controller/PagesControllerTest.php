<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 04.03.15
 * Time: 15:48
 */

namespace ZFS\Pages\Test\Controller;

use ZFS\Pages\Test\Bootstrap;
use ZFS\Pages\Controller\PagesController;
use Zend\Mvc\Router\RouteMatch;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use ZFS\Pages\Model\Object\Page as PageObject;

class PagesControllerTest extends \PHPUnit_Framework_TestCase
{
    /** @var $controller \ZFS\Pages\Controller\PagesController */
    protected $controller;
    /** @var $request \Zend\Http\Request */
    protected $request;
    /** @var $routeMatch \Zend\Mvc\Router\RouteMatch */
    protected $routeMatch;
    /** @var $event \Zend\Mvc\MvcEvent */
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();

        $this->controller = new PagesController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'pages'));
        $this->event      = new MvcEvent();
        $config           = $serviceManager->get('Config');
        $routerConfig     = isset($config['router']) ? $config['router'] : array();
        $router           = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);

        $serviceManager->setAllowOverride(true);
    }

    /**
     * @param mixed  $objectToReturn
     */
    protected function setUpPageGateway($objectToReturn = null)
    {
        $tableGateway = $this->getMockBuilder('ZFS\Pages\Model\Gateway\PageGateway')
            ->disableOriginalConstructor()
            ->getMock();

        $tableGateway->expects($this->any())
            ->method('selectOne')
            ->will($this->returnValue($objectToReturn));

        $this->controller->getServiceLocator()->setService('PageGateway', $tableGateway);
    }

    /**
     * @param string $slug
     * @param object $object
     * @param int    $expectedCode
     *
     * @dataProvider indexActionGetDataProvider
     */
    public function testIndexAction($slug, $object, $expectedCode)
    {
        $this->setUpPageGateway($object);

        $this->routeMatch->setParam('action', 'index');
        $this->routeMatch->setParam('slug', $slug);

        $this->controller->dispatch($this->request);

        $this->assertEquals($expectedCode, $this->controller->getResponse()->getStatusCode());
    }

    public function indexActionGetDataProvider()
    {
        return array(
            array(null, null, 404),
            array('foo', null, 404),
            array('foo', (object)array('content' => 'bar'), 200),
        );
    }


}
