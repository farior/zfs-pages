<?php

use ZFCTool\Service\Migration\AbstractMigration;

class Migration_20150303_102921_59 extends AbstractMigration
{

    /**
     * Upgrade
     */
    public function up()
    {
        $this->query("DROP TABLE IF EXISTS `zfs_pages`");
        $this->query("CREATE TABLE `zfs_pages` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `title` text NOT NULL,
          `slug` varchar(255) NOT NULL,
          `content` longtext,
          `keywords` text,
          `description` text,
          `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
          `user_id` int(10) unsigned NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`),
          UNIQUE KEY `slug` (`slug`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8");

        $this->query("INSERT INTO `zfs_pages` (`id`, `title`, `slug`, `content`, `keywords`, `description`, `created`, `updated`, `user_id`) VALUES ('3', 'About ZFStarter', 'about', '<p>ZFStarter is a content management framework based on Zend Framework 2. It was developed by PHP Team of <a href=\"http://nixsolutions.com/\">NIX Solutions Ltd</a>.</p>\r\n\r\n<p>Any questions, wishes, or offers? Feel free to contact us by <a href=\"mailto:ask@nixsolutions.com\">ask@nixsolutions.com</a></p>\r\n', '', '', '2015-03-11 12:39:51', '0000-00-00 00:00:00', '1')");
    }

    /**
     * Degrade
     */
    public function down()
    {
        $this->query("DROP TABLE IF EXISTS `zfs_pages`");
    }
}
