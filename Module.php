<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 17:17
 */

namespace ZFS\Pages;

use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\Stdlib\ArrayUtils;
use Zend\EventManager\EventInterface;
use ZFS\Pages\EventManager\PagesManagement as PagesManagementEvent;
use Zend\Mvc\MvcEvent;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{
    public function onBootstrap(EventInterface $e)
    {
        if (!$e instanceof MvcEvent) {
            return;
        }

        $event = new PagesManagementEvent($e);
        $event->renderSidebar();
    }

    public function getConfig()
    {
        $config = array();

        $configFiles = array(
            __DIR__ . '/config/module.config.php',
            __DIR__ . '/config/module.config.navigation.php',
        );

        // Merge all module config options
        foreach ($configFiles as $configFile) {
            $config = ArrayUtils::merge($config, include $configFile);
        }

        return $config;
    }
}
