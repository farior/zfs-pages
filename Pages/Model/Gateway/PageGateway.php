<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 18:38
 */

namespace ZFS\Pages\Model\Gateway;

use ZFS\Common\Model\Gateway\BaseGateway;
use Zend\Db\Sql\Expression;

class PageGateway extends BaseGateway
{
    public function getPages()
    {
        $sql = $this->getSql();

        $statement = $sql->prepareStatementForSqlObject($this->getPagesSelect());

        return $statement->execute();
    }

    /**
     * @return \Zend\Db\Sql\Select
     */
    public function getPagesSelect()
    {
        $sql = $this->getSql();

        $select = $sql->select()
            ->columns(array(
                'id',
                'title',
                'slug',
                'description',
                'created'
            ))
            ->join(
                array('users' => 'zfs_users'),
                'users.id = '.$this->table.'.user_id',
                array('name'),
                'Left'
            );

        return $select;
    }
}
