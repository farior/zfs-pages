<?php

namespace ZFS\Pages\Model\Gateway\Traits;

use ZFS\Pages\Model\Gateway\PageGateway;

trait GatewayTrait
{
    /**
     * @var PageGateway
     */
    protected $pageGateway = null;

    /**
     * Get page gateway
     *
     * @return PageGateway
     */
    public function getPageGateway()
    {
        if ($this->pageGateway == null) {
            $this->pageGateway = $this->getServiceLocator()->get('PageGateway');
        }

        return $this->pageGateway;
    }
}
