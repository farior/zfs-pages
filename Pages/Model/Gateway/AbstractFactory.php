<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 16:34
 */

namespace ZFS\Pages\Model\Gateway;

use ZFS\DomainModel\Gateway\AbstractFactory as BaseAbstractFactory;
use ZFS\DomainModel\Service\Options;

class AbstractFactory extends BaseAbstractFactory
{
    const PAGES_TABLE = 'zfs_pages';

    protected $provides = array(
        'PageGateway' => array(
            Options::OPTION_TABLE_NAME       => self::PAGES_TABLE,
            Options::OPTION_OBJECT_PROTOTYPE => 'ZFS\Pages\Model\Object\Page',
            Options::OPTION_TABLE_GATEWAY    => 'ZFS\Pages\Model\Gateway\PageGateway'
        )
    );
}
