<?php

namespace ZFS\Pages\Model\TransactionScript;

use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Db\Sql\Predicate\Like;
use ZFS\Pages\Model\Gateway\Traits\GatewayTrait;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class Pages implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait, GatewayTrait;

    public function getAll()
    {
        $sql = $this->getPageGateway()->getSql();

        $statement = $sql->prepareStatementForSqlObject($this->select());

        return $statement->execute();
    }

    /**
     * @param string $order
     * @param string $orderDirection
     * @param string $filterColumn
     * @param string $filter
     *
     * @return \Zend\Db\Sql\Select
     */
    public function select($order = null, $orderDirection = null, $filterColumn = null, $filter = null)
    {
        $sql = $this->getPageGateway()->getSql();

        $select = $sql->select()
            ->columns(array(
                'id',
                'title',
                'slug',
                'description',
                'created'
            ))
            ->join(
                array('users' => 'zfs_users'),
                'users.id = '.$this->getPageGateway()->table.'.user_id',
                array('name'),
                'Left'
            );

        if ($order) {
            $orderBy = $order;
            $orderDirection = $orderDirection ? : 'ASC';
            $select->order($orderBy . ' ' . $orderDirection);
        }

        if ($filterColumn && $filter) {
            $select->where(new Like($this->getPageGateway()->table.'.'.$filterColumn, '%'.$filter.'%'));
        }

        return $select;

    }

}
