<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 18:30
 */

namespace ZFS\Pages\Model\Object;

use ZFS\DomainModel\Object\ObjectMagic;

/**
 * @property int    id
 * @property string title
 * @property string slug
 * @property string content
 * @property string keywords
 * @property string description
 * @property string created
 * @property string updated
 * @property string userId
 */
class Page extends ObjectMagic
{
    protected $primaryColumns = array(
        'id'
    );

    protected $fieldToColumnMap = array(
        'userId' => 'user_id'
    );
}
