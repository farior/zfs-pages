<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 19:10
 */

namespace ZFS\Pages\Form\Management;

use Zend\Form\Form;

class Page extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden'
        ));

        $this->add(array(
            'type' => 'Text',
            'name' => 'title',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Title',
                'required' => true
            )
        ));

        $this->add(array(
            'type' => 'Text',
            'name' => 'slug',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Slug (Alias)',
                'maxlength' => '255',
                'required' => true
            )
        ));

        $this->add(array(
            'type' => 'Text',
            'name' => 'keywords',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Meta Keywords',
            )
        ));

        $this->add(array(
            'type' => 'Text',
            'name' => 'description',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => 'Meta Description',
            )
        ));

        $this->add(array(
            'name' => 'content',
            'type' => 'Textarea',
            'attributes' => array(
                'id' => 'editor',
                'placeholder' => 'Page content'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Save',
                'class' => 'btn btn-primary'
            )
        ));
    }
}
