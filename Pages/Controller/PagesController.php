<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 17:24
 */

namespace ZFS\Pages\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Response;
use ZFS\Pages\Model\Gateway\Traits\GatewayTrait;

class PagesController extends AbstractActionController
{
    use GatewayTrait;

    public function indexAction()
    {
        $slug = $this->params()->fromRoute('slug');

        $page = $this->getPageGateway()->selectOne(array('slug' => $slug));

        if (!$page) {
            $response = $this->getResponse();

            if ($response instanceof Response) {
                $response->setStatusCode(404);
            }

            return false;
        }

        return new ViewModel(array(
            'page' => $page
        ));
    }
}
