<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 02.03.15
 * Time: 17:24
 */

namespace ZFS\Pages\Controller;

use Zend\Db\Sql\Predicate\In;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use ZFS\Common\Controller\AbstractManagementController;
use Zend\View\Model\ViewModel;
use ZFS\Pages\Form\Management\Page as PageForm;
use ZFS\Pages\Model\Object\Page as PageObject;
use ZFS\Pages\Model\Gateway\Traits\GatewayTrait;
use Zend\Http\Request;

class PagesManagementController extends AbstractManagementController
{
    use GatewayTrait;

    /**
     * @permission pages_management
     * @see onDispatch()
     */
    public function indexAction()
    {
        $order          = $this->params()->fromQuery('order', null);
        $orderDirection = $this->params()->fromQuery('orderDirection', null);
        $filterColumn   = $this->params()->fromQuery('filterColumn', null);
        $filter         = $this->params()->fromQuery('filter', null);

        /** @var $pagesTS \ZFS\Pages\Model\TransactionScript\Pages */
        $pagesTS = $this->getServiceLocator()->get('PagesTransactionScript');

        $select = $pagesTS->select($order, $orderDirection, $filterColumn, $filter);

        $paginator = new Paginator(new DbSelect(
            $select,
            $this->getPageGateway()->getAdapter()
        ));

        $page = (int)$this->params()->fromQuery('page');

        $itemsPerPage = 10;

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($itemsPerPage);

        $viewModel = new ViewModel(array(
            'paginator'    => $paginator,
            'itemsPerPage' => $itemsPerPage,
            'query'        => $this->params()->fromQuery()
        ));

        $request = $this->getRequest();

        if (($request instanceof Request) && $request->isXmlHttpRequest()) {
            $viewModel->setTemplate('zfs/pages-management/table.phtml');
            $viewModel->setTerminal(true);
        }

        $this->getEventManager()->trigger('ZFS\Dashboard\Event\Navigation\Sidebar');

        return $viewModel;
    }

    /**
     * @permission pages_management
     * @see onDispatch()
     */
    public function createAction()
    {
        $pageId = (int)$this->params()->fromRoute('id');

        $pageForm = new PageForm('page');

        $request = $this->getRequest();

        if (($request instanceof Request) && $request->isPost()) {
            $pageForm->setData($request->getPost());

            if ($pageForm->isValid()) {
                $data = $pageForm->getData();

                $pageExist = $this->getPageGateway()->selectOne(array('slug' => $data['slug']));

                if (!$pageExist) {
                    $page = new PageObject();
                    $page->userId      = $this->identity()->userId;
                    $page->title       = $data['title'];
                    $page->slug        = $data['slug'];
                    $page->content     = $data['content'];
                    $page->keywords    = $data['keywords'];
                    $page->description = $data['description'];

                    if ($data['id']) {
                        $page->id = $data['id'];
                        $page->updated = date('Y-m-d H:i:s', strtotime('now'));

                        $this->getPageGateway()->updateObject($page);
                    } else {
                        $this->getPageGateway()->insertObject($page);
                    }

                    return $this->redirect()->toRoute('zfspagesmanagement');
                } else {
                    $this->flashMessenger()->addWarningMessage('page_exist', array('alias' => $data['slug']));
                }
            } else {
                foreach ($pageForm->getInputFilter()->getMessages() as $messages) {
                    foreach ($messages as $message) {
                        if (!is_array($message)) {
                            $this->flashMessenger()->addWarningMessage($message);
                        }
                    }
                }
            }
        }

        if ($pageId) {
            $page = $this->getPageGateway()->selectOne(array('id' => $pageId));

            if ($page) {
                $pageForm->setData($page->toArray());
            }
        }

        $this->getEventManager()->trigger('ZFS\Dashboard\Event\Navigation\Sidebar');

        return new ViewModel(array(
            'form' => $pageForm
        ));
    }

    /**
     * @permission user_management
     * @see onDispatch()
     */
    public function deleteAction()
    {
        $pageId = (int)$this->params()->fromRoute('id');

        if ($pageId) {
            $this->getPageGateway()->delete(array('id' => $pageId));
        }

        $request = $this->getRequest();

        if (($request instanceof Request) && $request->isPost()) {
            $ids = $request->getPost('ids');
            $this->getPageGateway()->delete(new In('id', explode(',', $ids)));
        }

        return $this->redirect()->toRoute('zfspagesmanagement');
    }

    /**
     * @codeCoverageIgnore
     */
    public function navigationAction()
    {
    }
}
