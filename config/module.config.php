<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 24.01.15
 * Time: 13:10
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'ZFS\Pages\Controller\Pages' => 'ZFS\Pages\Controller\PagesController',
            'ZFS\Pages\Controller\PagesManagement' => 'ZFS\Pages\Controller\PagesManagementController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'zfspagesmanagement' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/management/pages',
                    'defaults' => array(
                        'controller' => 'ZFS\Pages\Controller\PagesManagement',
                        'action'     => 'index',
                        'layout'     => 'layout/dashboard'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'create' => array(
                        'type' => 'Zend\Mvc\Router\Http\Literal',
                        'options' => array(
                            'route' => '/create',
                            'defaults' => array(
                                'action' => 'create',
                                'layout' => 'layout/dashboard'
                            ),
                        ),
                    ),
                    'edit' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/edit[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]*'
                            ),
                            'defaults' => array(
                                'action' => 'create',
                                'layout' => 'layout/dashboard'
                            )
                        )
                    ),
                    'delete' => array(
                        'type' => 'Zend\Mvc\Router\Http\Segment',
                        'options' => array(
                            'route' => '/delete[/:id]',
                            'constraints' => array(
                                'id' => '[0-9]*'
                            ),
                            'defaults' => array(
                                'action' => 'delete'
                            )
                        )
                    )
                )
            ),
            'zfspages' => array(
                'type' => 'Zend\Mvc\Router\Http\Regex',
                'options' => array(
                    'regex' => '/(?<slug>[a-zA-Z0-9_-]+)\.html',
                    'defaults' => array(
                        'controller' => 'ZFS\Pages\Controller\Pages',
                        'action'     => 'index',
                    ),
                    'spec' => '/%slug%',
                )
            )
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'ZFS\Pages\Model\Gateway\AbstractFactory'
        ),
        'invokables' => array(
            'PagesTransactionScript' => 'ZFS\Pages\Model\TransactionScript\Pages',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'template_path_stack' => array(
            __DIR__ . '/../view'
        ),
    ),
);
